﻿
//#include <iostream>

#include "hash.h"
#include "lookup8.c"

#define MAX_SLOT 64

//using std::cerr;
//using std::hex;

Item::Item( string key, string value){
	this->key = key;
	keyHash = hash( ((char *)key.data()), key.length(), 0xbeadfeed );
	this->value = value;
	
	//cerr << "item create\t" << hex << keyHash << '\t'<< '\n';
}


void HashTable::add( string key, string value){
	Item temp( key, value );
	unsigned long long index = hashmask(slots) & temp.keyHash;
	//cerr << "add: index\t" <<  hex << index << '\n';
	
	if ( !exists( temp ) ){
		table[ index ].push_back(temp);
		itemCount++;
		if ( itemCount > hashsize(slots)>>1 ){
			//cerr << "rehash needed \t"  << hashsize(slots) <<  '\t' <<  (hashsize(slots)>>1) <<  '\t' <<  itemCount<< '\n';		
			rehash( slots+1 );
		}
	}
}

void HashTable::rehash( unsigned int newSize ){
	if ( newSize > MAX_SLOT ) {
		newSize = MAX_SLOT;
	}
	if ( newSize != slots){
		list<Item>* newTable = new list<Item>[hashsize(newSize)];
		//cerr << "rehash\t" << hex << hashsize(slots) <<  '\t' << hex << itemCount << '\n';
		//cerr << "rehash\t"  << hashsize(slots) <<  '\t' <<  hashsize(newSize) <<  '\t' <<  itemCount << '\n';
	
		for( unsigned long long c = 0; c < hashsize(slots); c++){
			for( list<Item>::iterator i=table[ c ].begin(); i != table[ c ].end(); ++i){
				Item temp = *i;
				unsigned long long tempIndex = hashmask(newSize) & temp.keyHash;
				newTable[ tempIndex ].push_back(temp);
			}
			table[ c ].clear();
		}
		slots = newSize;
		delete [] table;
		table = newTable;
	}
}

bool HashTable::exists( string key, string value){
	//cerr << "HashTable::exists(s,s)\n";
	Item temp( key, value );
	return exists( temp );
}

bool HashTable::exists( Item item ){
	//cerr << "HashTable::exists(i)\n";
	list<Item>::iterator i;
	bool result = false;
	unsigned long long index = hashmask(slots) & item.keyHash;
	//cerr << "exists: index\t" <<  hex << index << '\n';
		
	for( i=table[ index ].begin(); i != table[ index ].end() && !result ; ++i){
		//cerr << "searching\n";
		result = ( i->key.compare(item.key) == 0);
	}
	return result;
}

HashTable::HashTable( unsigned int size ){
	slots = size;
	//cerr << "slots\t"  << hex << hashsize(slots) << '\n';
	//cerr << "hashmask\t" <<  hex << hashmask(slots) << '\n';
	table = new list<Item>[hashsize(slots)];
	itemCount = 0;
}


void HashSet::add( string key ){
	//ht.add(key,key);
	HashTable::add(key,key);
}

bool HashSet::exists( string key ){
	//return ht.exists(key,key);
	return HashTable::exists(key,key);
}

HashSet::HashSet(){
}


