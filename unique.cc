﻿
#include <iostream>

#include "hash.h"

int main(){
	HashSet hs;
	
	std::string line;
	while (std::getline(std::cin, line)){
		//std::cout << line << std::endl;
		
		if ( !hs.exists( line ) ){
			std::cout << line << '\n';
			hs.add(line);
		}
		
	}
	
}