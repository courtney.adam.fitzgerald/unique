#ifndef HASH_H
#define HASH_H 1

#include <string>
#include <list>

using std::string;
using std::list;

class Item{
	public:
		string key;
		unsigned long long keyHash;
		string value;
		Item( string key, string value);		
};

class HashTable{
	private:
		list<Item>* table;
		unsigned long long slots;
		bool exists( Item item );
		unsigned long long itemCount;
		void rehash( unsigned int newSize );
	public:
		void add( string key, string value);
		bool exists( string key, string value);		
		HashTable( unsigned int size=1 );
};


//class HashSet{
class HashSet:public HashTable{
	/*private:
		HashTable ht;*/
	public:
		void add( string key );
		bool exists( string key );
		HashSet( );
};



#endif
