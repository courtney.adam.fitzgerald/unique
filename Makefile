.PHONY: clean
SHELL := /bin/bash 
CFLAGS = -O3

all:	unique

unique: unique.o hash.o
	g++ $(CFLAGS) -o unique unique.o hash.o

%.o: %.cc 
	g++ $(CFLAGS) -c $*.cc

force:
	touch *.cc *.c; make
	
test: unique junk
	/usr/bin/time ./unique <junk 2> error | sort -n > result
	wc -l result junk
	tail -n2 error
		
junk: 
	cat <( yes a | head -n5000 ) <( yes b | head -n 10000 ) <( seq 15000 ) | sort -R > junk
	
clean:
	rm -f *.o junk error result unique
